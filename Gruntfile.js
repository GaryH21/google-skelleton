module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		compass: {
			build: {
				options: {
					sassDir: 'dev/assets/scss',
					cssDir: 'dev/assets/css' 
				}
			}
		},
		cssmin: {
			build: {
				files: {
					'live/assets/css/style.min.css': [
						'dev/assets/css/h5bp.css',
						'dev/assets/css/components.css',
						'dev/assets/css/main.css',
						'dev/assets/css/style.css'
					]
				}
			}
		},
		uglify: {
			build: {
				files: {
        			'live/assets/js/scripts.min.js':
        			[
        				'dev/assets/js/main.js',
        				'dev/assets/js/scripts.js'
        			]
      			}
			}
		},
		copy: {
			bower: {
				files: [{
					expand: true,
					cwd: 'bower_components/',
					src: '**',
					dest: 'live/assets/bower_components/'
				},
				{
					expand: true,
					cwd: 'bower_components/',
					src: '**',
					dest: 'dev/assets/bower_components/'
				}]
			},
			fonts: {
				expand: true,
				cwd: 'dev/assets/fonts/',
				src: '**',
				dest: 'live/assets/fonts/'
			},
			img: {
				expand: true,
				cwd: 'dev/assets/img/',
				src: '**',
				dest: 'live/assets/img/'
			},
			application: {
				expand: true,
				cwd: 'dev/application/',
				src: '**',
				dest: 'live/application/'
			},
			system: {
				expand: true,
				cwd: 'dev/system/',
				src: '**',
				dest: 'live/system/'
			},
			root: {
				expand: 'true',
				cwd: 'dev/',
				src: '*',
				dest: 'live/',
				filter: 'isFile'
			},
			htaccess: {
				expand: 'true',
				cwd: 'dev/',
				src: '.htaccess',
				dest: 'live/'
			}
		},
		clean: {
			fonts: {
      			force: 'true',
        		src: ['live/assets/fonts/**/*']
      		},
      		img: {
      			force: 'true',
        		src: ['live/assets/img/**/*']
      		},
      		style: {
      			force: 'true',
        		src: ['live/assets/style/css/**/*']
      		},
      		application: {
      			force: 'true',
        		src: ['live/application/**/*']
      		},
      		system: {
      			force: 'true',
        		src: ['live/system/**/*']
      		},
      		root: {
      			force: 'true',
        		src: ['live/*.*']
      		},
      		live: {
      			force: 'true',
        		src: ['live/**/*']	
      		}
    	},
		watch: {
			css: {
    			files: ['dev/assets/css/*.css'],
    			tasks: ['cssmin'],
    			options: {
      				livereload: 35729
    			}
    		},
    		scss: {
    			files: ['dev/assets/scss/*.scss'],
    			tasks: ['compass'],
    			options: {
      				livereload: 35729
    			}
    		},
    		scripts: {
    			files: ['dev/assets/js/*.js'],
    			tasks: ['uglify:build'],
    			options: {
      				livereload: 35729
    			}
    		},
    		application: {
    			files: ['dev/application/**/*'],
    			tasks: ['clean:application', 'copy:application'],
    			options: { livereload: 35729 }
    		},
    		system: {
    			files: ['dev/system/**/*'],
    			tasks: ['clean:system', 'copy:system'],
    			options: { livereload: 35729 }
    		},
    		root: {
    			files: ['dev/*'],
    			tasks: ['clean:root', 'copy:root'],
    			options: { livereload: 35729 }
    		}
    	}
	});

	grunt.registerTask('default', ['clean:live', 'uglify', 'compass', 'cssmin', 'copy', 'watch']);
}