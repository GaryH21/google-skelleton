<?php

class dashboard extends CI_Controller {

	public function index() {
		$this->load->view("templates/header");
		$this->load->view("dashboard/index");
		$this->load->view("templates/footer");
	}

}