<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Starter Kit</title>
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="196x196" href="/assets/img/touch/chrome-touch-icon-196x196.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <meta name="msapplication-TileImage" content="/assets/img/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">
    <link rel="stylesheet" href="/assets/css/style.min.css">
  </head>
  <body ng-app="finance">
    <header class="app-bar promote-layer">
      <div class="app-bar-container">
        <button class="menu"><img src="/assets/img/hamburger.svg" alt="Menu"></button>
        <h1 class="logo">Web Starter Kit</h1>
        <section class="app-bar-actions">
        <!-- Put App Bar Buttons Here -->
        </section>
      </div>
    </header>

    <nav class="navdrawer-container promote-layer">
      <h4>Navigation</h4>
      <ul>
        <li><a href="/dashboard">Dashboard</a></li>
      </ul>
    </nav>